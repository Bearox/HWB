#ifndef __ROUTE_H__
#define __ROUTE_H__

#include "lib_io.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <queue>
#include <stack>
#include <vector>
#include <map>
#include <cmath>
#include <sys/time.h>
#include <cstdlib>
using namespace std;


const int MAX_NODE = 1001;
const int MAX_LINK = 21;
const int MAX_CONS = 501;

struct Link
{
        int start;
	int end;
	int band;
	int cost;
};

struct Node
{
        int id;
	int count;
        map<int, struct Link*> links;
	int aspect;
	float score;
        bool isConsume;
};

struct Consume
{
        int id;
        int nodeID;
        int band;
        int rBand;
};

struct Route
{
        int len;
        int band;
        int cost;
        int consumeID;
        int serverID;
        vector<int> route_node;
};

void deploy_server(char * graph[MAX_EDGE_NUM], int edge_num, char * filename);
void init();
void get_number(char *line, int *tmp, int len);
void read_data(char * topo[MAX_EDGE_NUM], int line_num);
void caculate_score();
int  find_server();
void create_graph();
bool find_route(int serverID, int &endID);
void update_ans(int start, int end, struct Route &rt);
void update_map();
void last_ans(struct Route &rt);
void print_ans(char *filename);
void back_map(std::vector<struct Route *>::iterator ite);
int get_answer_cost();
void update_score();
void update_answer();
void filter_answer(float &s);


#endif
